#include<stdio.h>
#include<math.h>
int main()
{
    float a,b,c,d,r1,r2;
    //clrscr()
    printf("Enter  coefficients a,b,c\n");
    scanf("%f %f %f",&a,&b,&c);
    if (a==0){
        printf("Error, a cannot be zero:");
        return 1;
    }
    // calculating the discriminant
    d=b*b - 4*a*c;
    
    if (d==0)
    {
        //real and distinct roots
        printf("\nRoots are real and equal");
        r1=-b /(2*a);
        r2=-b /(2*a);
        printf("\nFirst root =%f and second root =%f ",r1,r2);
    } 
    else if (d>0)
    {
     // real and equal roots
        printf("\n roots are real and distinct");
       r1=(-b +sqrt(d))/(2*a);
       r2=(-b-sqrt(d))/(2*a);
       printf("\nFirst  root =%f and second root =%f ",r1,r2); 
    } 
    else 
    {
        // complex roots
        printf("\n Roots are complex");
        

    }
    return 0;
}